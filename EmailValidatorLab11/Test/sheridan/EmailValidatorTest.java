package sheridan;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import sheridan.EmailValidator;

/**
 * 
 */

/**
 * @author bhatt
 *
 */
public class EmailValidatorTest {

	// @ Annotation Tests

	@Test
	public void tesEmailValidatorRegular() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gmail.com");
		assertTrue("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorExceptional() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23gmail.com");
		assertFalse("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorBoundaryIn() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gmail.co.in");
		assertTrue("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorBoundaryOut() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23gmail.c@om");
		assertFalse("Invalid Email.", isValidEmail);
	}

	// One @ Annotation

	@Test
	public void tesEmailValidatorOneAnnotationRegular() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gmail.com");
		assertTrue("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorOneAnnotationExceptional() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gmail@.com");
		assertFalse("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorOneAnnotationBoundaryOut() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhatt@harshal23@gmail@.com");
		assertFalse("Invalid Email.", isValidEmail);
	}

	// Account Section Tests

	@Test
	public void tesEmailValidatorAccountRegular() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gmail.com");
		assertTrue("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorAccountExceptional() {
		boolean isValidEmail = EmailValidator.isValidEmail("BhattHarshal23@gmail.com");
		assertFalse("Invalid Email.", isValidEmail);
	}

	// Domain Section tests

	@Test
	public void tesEmailValidatorDomainRegular() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gmail.com");
		assertTrue("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorDomainExceptional() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@Gmail.com");
		assertFalse("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorDomainBoundaryOut() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gm4Il.com");
		assertFalse("Invalid Email.", isValidEmail);
	}

	// Extension Section Tests

	@Test
	public void tesEmailValidatorExtensionRegular() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gmail.com");
		assertTrue("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorExtensionExceptional() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gmail.Com");
		assertFalse("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorExtensionBoundaryOut() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gmail.Co2");
		assertFalse("Invalid Email.", isValidEmail);
	}

	// Starting with numbers tests

	@Test
	public void tesEmailValidatorStartingWithNumbersRegular() {
		boolean isValidEmail = EmailValidator.isValidEmail("bhattharshal23@gmail.com");
		assertTrue("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorStartingWithNumbersExceptional() {
		boolean isValidEmail = EmailValidator.isValidEmail("22bhattharshal23.@gmail.com");
		assertFalse("Invalid Email.", isValidEmail);
	}

	@Test
	public void tesEmailValidatorStartingWithNumbersBoundaryIn() {
		boolean isValidEmail = EmailValidator.isValidEmail("b22hattharshal23@gmail.com");
		assertTrue("Invalid Email.", isValidEmail);
	}
}
